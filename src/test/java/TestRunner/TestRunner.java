package TestRunner;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/feature",
        glue= {
                "StepDefinition",
                "Automation.Practice.general",
                "ApplicationPages",
        },
        // plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"
        // "pretty",
        // "target/cucumber-reports/report.html"
        //    "json:target/cucumber.json",
        //   "html:target/site/cucumber-pretty"*/
        //  },

        tags = {" @LoginAutomationPractice"} //useful for test development, please leave.
)
public class TestRunner {

}




