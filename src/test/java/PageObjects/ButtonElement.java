package PageObjects;

import Helpers.SeleniumHelper;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

/**
 * This element represents a Button. It supports getting the button text, and clicking the button.
 **/
public class ButtonElement extends SeleniumHelper {

    public static final long WAIT_TIME_IN_SECONDS = 30L;

    public ButtonElement(FindElementOnPageBy findingBy, String finder) {
        super( FindElementOnPageBy.convert( findingBy, finder ) );
    }

    public void click() {
        await().atMost( WAIT_TIME_IN_SECONDS, SECONDS ).until( this::clickElement );
    }

    public boolean isClickable() {
        return getElement().isEnabled();
    }


}
