package PageObjects;


import Helpers.SeleniumHelper;
import org.openqa.selenium.StaleElementReferenceException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

/**
 * Provides access to read only text on the screen.
 */
public class ReadOnlyTextBoxElement extends SeleniumHelper {

    public ReadOnlyTextBoxElement(FindElementOnPageBy findingBy, String finder) {
        super(FindElementOnPageBy.convert(findingBy, finder));
    }

    public String getText() {
        return Optional.of(getElement().getText())
                .filter(t -> !t.equals(""))
                .orElse(Optional.ofNullable(getElement().getAttribute("value")).orElse(""));
    }

    public void waitForText(String expectedText) {
        await().atMost(3, TimeUnit.SECONDS)
                .ignoreException(StaleElementReferenceException.class)
                .until(() -> getText().equals(expectedText));
    }
}
