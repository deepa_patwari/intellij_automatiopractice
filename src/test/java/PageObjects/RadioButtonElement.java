package PageObjects;
import Helpers.SeleniumHelper;

/**
 * Represents a single radio button, such as yes, on the page.
 */
public class RadioButtonElement extends SeleniumHelper {


    public RadioButtonElement(FindElementOnPageBy findingBy, String finder) {
        super( FindElementOnPageBy.convert( findingBy, finder ) );
    }

    public void select() {
        if (getElement().isSelected()) {
            return;
        }

        getElement().click();
    }

    public void unselect() {
        if (getElement().isSelected()) {
            getElement().click();
        }
    }

    public boolean isSelected() {
        return getElement().isSelected();
    }


}
