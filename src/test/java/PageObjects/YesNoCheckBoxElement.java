package PageObjects;



public class YesNoCheckBoxElement {

    private final RadioButtonElement noSelect;
    private final RadioButtonElement yesSelect;

    public YesNoCheckBoxElement(RadioButtonElement noSelect, RadioButtonElement yesSelect) {
        this.noSelect = noSelect;
        this.yesSelect = yesSelect;
    }

    public void selectYes() {
        yesSelect.select();
        noSelect.unselect();
    }

    public void selectNo() {
        noSelect.select();
        yesSelect.unselect();
    }

    public void clearSelection() {
        noSelect.unselect();
        yesSelect.unselect();
    }

    public boolean isNoSelected() {
        return noSelect.getElement().isSelected();
    }

    public boolean isYesSelected() {
        return yesSelect.getElement().isSelected();
    }

}
