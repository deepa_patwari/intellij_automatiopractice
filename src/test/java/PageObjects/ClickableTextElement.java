package PageObjects;

import Helpers.SeleniumHelper;

import java.util.Optional;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

/**
 * This element represents a text element that contains a URL (for instance an </a> tag)
 */
public class ClickableTextElement extends SeleniumHelper {

    public static final long WAIT_TIME_IN_SECONDS = 30L;

    public ClickableTextElement(FindElementOnPageBy findingBy, String finder) {
        super( FindElementOnPageBy.convert( findingBy, finder ) );
    }

    public void clickText() {
        await().atMost( WAIT_TIME_IN_SECONDS, SECONDS ).until( this::clickElement );
    }

    public void clickTextCustomTimeout(int secsTillTimeout) {
        await().atMost( secsTillTimeout, SECONDS ).until( this::clickElement );
    }

    public String getText() {
        return Optional.of( getElement().getText() )
                .filter( t -> !t.equals( "" ) )
                .orElse( Optional.ofNullable( getElement().getAttribute( "value" ) ).orElse( "" ) );
    }

    public String getActivitiLinkURL() {
        return getElementpresenceLocated().getAttribute( "ng-href" );
    }

    public String getAttribute(String attribute) {
        return getElementpresenceLocated().getAttribute( attribute );
    }

}