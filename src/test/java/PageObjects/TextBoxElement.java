package PageObjects;


import Helpers.SeleniumHelper;
import org.openqa.selenium.StaleElementReferenceException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
/**
 * This element represents a text box on the screen. You can only set/get text.
 */
public class TextBoxElement extends SeleniumHelper {
    public static final long WAIT_TIME_IN_SECONDS = 30L;

    private Optional<String> classPresentWhenError = Optional.empty();

    public TextBoxElement(FindElementOnPageBy findingBy, String finder) {
        super(FindElementOnPageBy.convert(findingBy, finder));
    }

    public TextBoxElement(FindElementOnPageBy findingBy, String finder, String classPresentWhenError) {
        this(findingBy, finder);
        this.classPresentWhenError = Optional.of(classPresentWhenError);
    }

    public void clearText() {
        getElement().clear();
    }

    public String getText() {
        return Optional.of(getElement().getText())
                .filter(t -> !t.equals(""))
                .orElse(Optional.ofNullable(getElement().getAttribute("value")).orElse(""));
    }

    public void setText(String text) {
      //  clearText();
        if (text != null) {
            getElement().sendKeys(text);
        }
    }

    public boolean hasError() {
        return classPresentWhenError
                .map(c -> getElement().getAttribute("class").contains(c))
                .orElse(false);
    }

    public boolean isRequired(){
        return getElement().getAttribute("class").contains("ng-invalid");
    }

    public void clickText() {
        await().atMost(WAIT_TIME_IN_SECONDS, SECONDS).until(this::clickElement);
    }

    public void clickTextCustomTimeout(int secsTillTimeout) {
        await().atMost(secsTillTimeout, SECONDS).until(this::clickElement);
    }

    public String getActivitiLinkURL(){
        return getElementpresenceLocated().getAttribute("ng-href");
    }

    public void waitForText(String expectedText) {
        await().atMost(3, TimeUnit.SECONDS)
                .ignoreException(StaleElementReferenceException.class)
                .until(() -> getText().equals(expectedText));
    }




}