package PageObjects;

import Helpers.SeleniumHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.List;
import java.util.stream.Collectors;


/**
 * This element represents a drop down list. You can select an item in the dropdown list and view all available items.
 */
public class DropdownListElement extends SeleniumHelper {

    public DropdownListElement(FindElementOnPageBy findingBy, String finder) {
        super( FindElementOnPageBy.convert( findingBy, finder ) );
    }

    public void selectItem(String itemText) {
        new Select( getElement() ).selectByVisibleText( itemText );
    }

    public String getSelectedItem() {
        return new Select( getElement() ).getFirstSelectedOption().getText();
    }

    public List<String> getAllItems() {
        return getElement()
                .findElements( By.tagName( "option" ) )
                .stream()
                .map( WebElement::getText )
                .collect( Collectors.toList() );
    }
}
