package Helpers;
import Core.AppDriver;
import PageObjects.FindElementOnPageBy;
import org.apache.log4j.spi.LoggerFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;


public class SeleniumHelper extends AppDriver{
	private static final Class<LoggerFactory> LOGGER = LoggerFactory.class;
	private static final int DEFAULT_POLL_INTERVAL = 25;
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static SimpleDateFormat dateFormat1 = new SimpleDateFormat("D-M-YYYY hh:mm");
	private By finder;

	protected SeleniumHelper(By finder) {
		this.finder = finder;
	}

	public static Date today() {
		final Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}

	public WebElement getElement() {
		//LOGGER.debug("getting element by: {}", finder);
		WebDriver driver = getDriver();
		//System.out.println(System.getProperty("user.dir"));
		WebElement element = findVisibleElement();
	//	LOGGER.debug("found visible element by: {}", finder);
		new Actions(driver).moveToElement(element).build().perform();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: 'end', inline: 'start'});", element);
	//	LOGGER.debug("successfully moved to element: {}", finder);
		return element;
	}

	public List<WebElement> getChildElements(FindElementOnPageBy childFinderBy, String childFinder) {
		getElement();
		WebDriver driver = getDriver();
		return driver.findElement(finder).findElements(FindElementOnPageBy.convert(childFinderBy, childFinder));
	}

	public List<WebElement> getAllElements() {
		WebDriver driver = getDriver();
		return driver.findElements(finder);
	}

	public List<WebElement> getWebElements() {
		WebDriver driver = getDriver();
		findVisibleElement();
		return driver.findElements(finder);
	}

	private WebElement findVisibleElement() {
		WebDriver driver = getDriver();

		return new FluentWait<>(driver)
				.withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofMillis(DEFAULT_POLL_INTERVAL))
				.ignoring(WebDriverException.class)
				.until(visibilityOfElementLocated(finder));
	}

	public boolean isDisplayed() {

		WebDriver driver = getDriver();
		return !driver.findElements(finder).isEmpty();
	}

	public boolean isVisible() {
		WebDriver driver = getDriver();
		List<WebElement> elements = driver.findElements(finder);
		if (!elements.isEmpty()) {
			return elements.stream().anyMatch(WebElement::isDisplayed);
		} else {
			return false;
		}
	}

	public WebElement getElementpresenceLocated() {
		WebDriver driver = getDriver();

		return new FluentWait<>(driver)
				.withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofMillis(DEFAULT_POLL_INTERVAL))
				.ignoring(WebDriverException.class)
				.until(presenceOfElementLocated(finder));

	}

	public boolean isElementNotPresentAndDisplayed() {
		WebDriver driver = getDriver();
		try {
			return new FluentWait<>(driver)
					.withTimeout(Duration.ofSeconds(30))
					.pollingEvery(Duration.ofMillis(3))
					.ignoring(WebDriverException.class)
					.until(ExpectedConditions.invisibilityOfElementLocated(finder));
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isDisplayedAfterWait() {
		WebDriver driver = getDriver();
		try {
			return new FluentWait<>(driver)
					.withTimeout(Duration.ofSeconds(10))
					.pollingEvery(Duration.ofMillis(3))
					.ignoring(WebDriverException.class)
					.until(ExpectedConditions.not(ExpectedConditions.invisibilityOfElementLocated(finder)));
		} catch (Exception e) {
			return false;
		}
	}

	public boolean clickElement() {
		try {
			getElement().click();
		} catch (WebDriverException wde) {
			return false;
		}

		return true;
	}

}