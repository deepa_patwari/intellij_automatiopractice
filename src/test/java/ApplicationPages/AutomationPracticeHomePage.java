package ApplicationPages;

import PageObjects.FindElementOnPageBy;
import PageObjects.ReadOnlyTextBoxElement;
public class AutomationPracticeHomePage {

    private AutomationPracticeHomePage() {}


    public static ReadOnlyTextBoxElement My_Account = new ReadOnlyTextBoxElement(FindElementOnPageBy.XPATH,("//*[@id='center_column']/h1"));
}
