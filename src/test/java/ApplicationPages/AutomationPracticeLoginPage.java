package ApplicationPages;

import PageObjects.ButtonElement;
import PageObjects.FindElementOnPageBy;
import PageObjects.TextBoxElement;

public class AutomationPracticeLoginPage {

    private AutomationPracticeLoginPage() {}

    public static ButtonElement signin = new ButtonElement(FindElementOnPageBy.CLASSNAME,"login");
    public static TextBoxElement U_name = new TextBoxElement(FindElementOnPageBy.ID, "email");
    public static TextBoxElement Pwd = new TextBoxElement(FindElementOnPageBy.ID, "passwd");
    public static ButtonElement LoginButton = new ButtonElement(FindElementOnPageBy.ID,"SubmitLogin");

}
