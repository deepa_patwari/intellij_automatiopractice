package StepDefinition;


import Core.AppDriver;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import ApplicationPages.*;

public class LoginAutomationPractice {

	@Given("^I load the url for Automation Practice$")
	public void i_load_the_url_for_Automation_Practice()  {
		AppDriver.NavigatetoHomePage(); ;
		
	}	
	
	@And("^I click on signin link$")
	public void iClickOnSignInLink() throws Throwable {
		Thread.sleep(1000);
		AutomationPracticeLoginPage.signin.click();
	}
	
	@And("^Enter the username and password(.*) (.*) (.*)$")
	public void iEnterUsernameAndPassword(String TestData, int row,String Sheet1) throws IOException {
		String [] Testdata =TestUtils.TestUtils.readExcel(TestData, row, Sheet1);
		AutomationPracticeLoginPage.U_name.setText(Testdata[1]);
		AutomationPracticeLoginPage.Pwd.setText(Testdata[2]);

	}
	
	@And("^I click on login button$")
	public void iClickOnAddButton() throws InterruptedException {
		Thread.sleep(1000);
		AutomationPracticeLoginPage.LoginButton.click();
		AppDriver.captureScreen();
		
	}
	@Then("^I navigate to the homepage$")
	public void i_navigate_to_the_homepage() throws Throwable {
		String Logo = AutomationPracticeHomePage.My_Account.getText();
		assertEquals(Logo,"MY ACCOUNT");
	
	
	}
}


