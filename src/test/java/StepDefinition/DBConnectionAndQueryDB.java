package StepDefinition;

import cucumber.api.java.en.Given;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBConnectionAndQueryDB {

	@Given("^Connected to the mysql server and PetClinic database(.*)$")
	public void connected_to_the_mysql_server_and_PetClinic_database(String password) throws SQLException, Throwable

	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		String username1 = "root";
		String password1 = password;
		String dbURL = "jdbc:mysql://127.0.0.1:3306/petclinic?useLegacyDatetimeCode=false&serverTimezone=America/New_York";
		Connection con = DriverManager.getConnection(dbURL,username1,password1);
		//Creating statement object
		Statement stmt = con.createStatement();
		String selectquery = "Select * from owners";
		ResultSet rs = stmt.executeQuery(selectquery);
		while (rs.next()) {
			String name = rs.getString(2);
			System.out.println(name);
			break;
		}
		//Closing DB Connection
		con.close();
	}

}


