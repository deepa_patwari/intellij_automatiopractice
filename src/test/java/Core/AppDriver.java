package Core;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AppDriver {
    public static final String TIME_OUT_IN_SECONDS = "SLEEP";
    public static final String LONG_SLEEP = "LONGSLEEP";
    public static final String HOME_PAGE = "HOME_PAGE";
    protected static final PropertyBag propertyBag = PropertyBag.getInstance();
    private static final String BROWSER_TYPE = "BROWSER_TYPE";
    private static final String PRINT_SCREEN_ON_ERROR = "PRINT_SCREEN_ON_ERROR";
    protected static String PRINT_SCREEN_LOCATION = "PRINT_SCREEN_LOCATION";
    private static Map<String, WebDriver> Drivers = new HashMap<String, WebDriver>();
    protected static WebDriver driver;
    static {
        try {
                if (propertyBag.getConfigProperty(BROWSER_TYPE).equalsIgnoreCase(Broswer.IE.name())) {
                String exePath = "C:\\IEDriver\\IEDriverServer.exe";
                System.setProperty("webdriver.ie.driver", exePath);
                driver = new InternetExplorerDriver();
                Drivers.put("IE", driver);
            } else if (propertyBag.getConfigProperty(BROWSER_TYPE).equalsIgnoreCase(Broswer.CHROME.name())) {
                String exePath = "C:\\Users\\deepa\\Chrome\\chromedriver.exe";
                System.setProperty("webdriver.chrome.driver", exePath);
                driver = new ChromeDriver();
                Drivers.put("Chrome", driver);
            }
            if (driver != null) {
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

            }
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public static WebDriver getDriver() {
        return driver;

    }

    public  static void NavigatetoHomePage(){
         driver.navigate().to(propertyBag.getConfigProperty(HOME_PAGE));

    }

    public static String captureScreen(){
        String path;
        try {
            File source = ((TakesScreenshot)driver).getScreenshotAs( OutputType.FILE);
            String fileExtension = source.getName().substring((source.getName().lastIndexOf(".")));
            path = PRINT_SCREEN_LOCATION +  fileExtension;
            FileUtils.copyFile(source, new File(path));
        }
        catch(IOException e) {
            path = "Failed to capture screenshot: " + e.getMessage();
            System.out.println("Failed to capture screenshot: " + e.getMessage());
        }
        return path;
    }

    public static void CloseAllDrivers()
    {
        for (String key : Drivers.keySet())
            Drivers.keySet().clear();
    }
    enum Broswer {
         IE, CHROME
    }
}







