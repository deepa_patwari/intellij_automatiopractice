package Core;
import java.io.IOException;
import java.util.Properties;
public class PropertyBag {

        private static final String CONFIG_PROPERTY_PATH  = "src//test//resources//configs//Config.properties";
        private Properties properties = new Properties();

        private PropertyBag() {

            try {
                properties.load(TestUtils.TestUtils.getFileStream(CONFIG_PROPERTY_PATH));
            } catch (IOException e) {
                throw new ExceptionInInitializerError(e);
            }
        }

        public static PropertyBag getInstance() {
            return SingletonHolder.INSTANCE;
        }

        public String getConfigProperty(String key) {
            return properties.getProperty(key).trim();
        }

        private static class SingletonHolder {
            private static final PropertyBag INSTANCE = new PropertyBag();
        }

}
