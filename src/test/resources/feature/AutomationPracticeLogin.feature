 @LoginAutomationPractice
 Feature: AutomationPractice Login Test
   Description: The purpose of this feature is to Login to Automation Practice application
 Scenario Outline: Load Automation Practice application
    Given I load the url for Automation Practice
    When I click on signin link 
    And Enter the username and password <TestData> <Row> <Sheet>
    Then I click on login button
    And I navigate to the homepage
    
   
   Examples:
   |TestData|Row|Sheet|
   |output.xlsx|0|Sheet1|
 

