package TestUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

/***************** Excel imports**********************/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestUtils {
    public static String[] Data = new String[5];
    public static String TestDatafilePath = "C://Users//deepa//git//repository//Practice//src//test//resources//datafiles";
    private static int num = 1;
    private static Object String;

    public static FileInputStream getFileStream(String path) {


        FileInputStream inputStream = null;
        try {

            inputStream = new FileInputStream( new File( path ) );
        } catch (Exception ex) {

        }
        return inputStream;
    }

    /********************************************* Excel Read and Write
     * @param Sheet1
     * @return
     * @return
     * @return
     * @throws IOException *********************************************************************/


    public static String[] readExcel(String fileName, int row, String Sheet1) throws IOException {


        TestDatafilePath = TestDatafilePath.trim();
        fileName = fileName.trim();
        String filepath = TestDatafilePath + "//" + fileName;
        File file = new File( filepath );
        FileInputStream inputStream = new FileInputStream( file );
        XSSFWorkbook wb = new XSSFWorkbook( inputStream );
        XSSFSheet sheet = wb.getSheet( Sheet1 );
        XSSFRow Row;
        Row = sheet.getRow( row );
        for (int i = 0; i <= Row.getLastCellNum() - 1; i++) {
            Data[num] = Row.getCell( i ).getStringCellValue();
			/* Always The num value when incremented inside the for loop should
		 be equal to the value of row.getlastcellnum() value and not exceed it, otherwise it will error and initialize it to 1*/
            num += num;
        }

        return Data;
    }

	/*public static String[] returnnames() {
	   return  Data;
	}*/


    /******************************************Write excel file**************************************************************************/

    public static void writeExcel(String filePath, String sheetName, String firstname, String lastname) throws IOException {

        File file = new File( filePath );
        FileInputStream inputStream = new FileInputStream( file );
        XSSFWorkbook wb = new XSSFWorkbook( inputStream );
        XSSFCell cell;
        XSSFSheet sheet = wb.getSheet( sheetName );
        int row = 0;
        XSSFRow newRow = sheet.createRow( row );
        //Fill data in row
        cell = newRow.createCell( 0 );
        cell.setCellValue( firstname );
        cell = newRow.createCell( 1 );
        cell.setCellValue( lastname );
        inputStream.close();
        FileOutputStream outputStream = new FileOutputStream( file );
        wb.write( outputStream );
        outputStream.close();
    }

    /********************************** Read and Write Text file*****************************************/

    public static void writetext(String outputFile, String saveValue) {
        try {
            OutputStreamWriter writer = new OutputStreamWriter( new FileOutputStream( outputFile, false ), "UTF-8" );
            BufferedWriter fbw = new BufferedWriter( writer );
            fbw.write( saveValue );
            System.out.println( "Written the value into the text file" + " " + saveValue );
            fbw.newLine();
            fbw.close();
        } catch (Exception e) {
            System.out.println( "Error: " + e.getMessage() );
        }

    }

}
/*******************************Read Text file
 * @throws Throwable ********************************************************/

/*public static String readtext(String outputFile) throws Throwable {

	 BufferedReader br = new BufferedReader(new FileReader(outputFile));
	  String req;
	  while ((req= br.readLine()) != null)
	String  String = req;
	   System.out.println(" This is from the output file ~~~~"+" "+ String);
	return String;
}
}*/
